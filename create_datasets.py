#!/usr/bin/env python3
import os
import pandas as pd
from sklearn.model_selection import train_test_split
from utils import normalize_min_max


def load_data_single_prs(path, disease):
    included_columns = [f"pheno_{disease}", f"prs_{disease}", "british", "sex_f31_0_0", "age_when_attended_assessment_centre_f21003_0_0", "Array"]
    included_columns.extend([f"genetic_principal_components_f22009_0_{x}" for x in range(1,11)])
    df = pd.read_csv(path, usecols=included_columns, sep="\t")

    # Use only british data 
    df = df[df["british"] == 1]    
    df = df.drop(columns=["british"])

    # Use one-hot encoding for columns sex and array
    df = pd.get_dummies(df, columns=["sex_f31_0_0"])
    df = pd.get_dummies(df, columns=["Array"])

    return df


def load_data_multi_prs(path, disease):
    included_columns = [f"pheno_{disease}", f"prs_{disease}", "british", "sex_f31_0_0", "age_when_attended_assessment_centre_f21003_0_0", "Array"]
    included_columns.extend([f"genetic_principal_components_f22009_0_{x}" for x in range(1,11)])
    list_all_PGS = ["PGS000004","PGS000005","PGS000006","PGS000007","PGS000008","PGS000009","PGS000015","PGS000025","PGS000043","PGS000045","PGS000046","PGS000047","PGS000048","PGS000049","PGS000050","PGS000052","PGS000053","PGS000054","PGS000055","PGS000058","PGS000064","PGS000065","PGS000066","PGS000068","PGS000069","PGS000070","PGS000071","PGS000072","PGS000074","PGS000075","PGS000076","PGS000077","PGS000078","PGS000079","PGS000080","PGS000081","PGS000082","PGS000083","PGS000084","PGS000085","PGS000086","PGS000087","PGS000088","PGS000089","PGS000090","PGS000091","PGS000092","PGS000093","PGS000094","PGS000095","PGS000096","PGS000097","PGS000098","PGS000099","PGS000100","PGS000101","PGS000102","PGS000103","PGS000104","PGS000105","PGS000106","PGS000107","PGS000108","PGS000109","PGS000110","PGS000111","PGS000112","PGS000113","PGS000115","PGS000124","PGS000146","PGS000151","PGS000163","PGS000164","PGS000165","PGS000166","PGS000167","PGS000168","PGS000169","PGS000170","PGS000171","PGS000172","PGS000173","PGS000174","PGS000175","PGS000176","PGS000177","PGS000178","PGS000179","PGS000180","PGS000181","PGS000182","PGS000183","PGS000184","PGS000185","PGS000186","PGS000187","PGS000188","PGS000189","PGS000190","PGS000191","PGS000193","PGS000210","PGS000212","PGS000213","PGS000214","PGS000215","PGS000216","PGS000297","PGS000298","PGS000299","PGS000300","PGS000301","PGS000302","PGS000303","PGS000304","PGS000305","PGS000306","PGS000307","PGS000308","PGS000309","PGS000310","PGS000311","PGS000312","PGS000313","PGS000314","PGS000315","PGS000318","PGS000319","PGS000321","PGS000322","PGS000323","PGS000324","PGS000325","PGS000326","PGS000334","PGS000336","PGS000338","PGS000339"]
    included_columns.extend(list_all_PGS)
    df = pd.read_csv(path, usecols=included_columns, sep="\t")

    # Use only british data 
    df = df[df["british"] == 1]    
    df = df.drop(columns=["british"])

    # Use one-hot encoding for columns sex and array
    df = pd.get_dummies(df, columns=["sex_f31_0_0"])
    df = pd.get_dummies(df, columns=["Array"])

    return df


def female_only_for_bc_data(df):
    # Use only female data for breast cancer
    df = df[df["sex_f31_0_0_Female"] == 1]
    df = df.drop(columns=["sex_f31_0_0_Female", "sex_f31_0_0_Male"])
    
    return df


def split_off_categorical_data(df):
    categorical_columns = df[["sex_f31_0_0_Female", "sex_f31_0_0_Male", "Array_Batch", "Array_UKBiLEVEAX", f"pheno_{disease}"]]
    df = df.drop(columns=["sex_f31_0_0_Female", "sex_f31_0_0_Male", "Array_Batch", "Array_UKBiLEVEAX", f"pheno_{disease}"])
    return (df, categorical_columns)


if __name__ == "__main__":
    path = "/PATH/TO/DATA/"
    output_path = "/OUTPUT/PATH/"
    diseases = ["cad", "bc", "t2d"]
    seed = 38565
      


    for seed in [38565, 22656, 81660]:
        for disease in diseases:
            df = load_data_single_prs(path, disease)

            # Split off categorical data, normalize remaining data, merge again
            df, categorical_columns = split_off_categorical_data(df)
            df = normalize_min_max(df)
            df = df.join(categorical_columns)
         
            if disease == "bc":
                df = female_only_for_bc_data(df)

            # Split off phenotype of investigated disease
            phenotypes = df[f"pheno_{disease}"]
            df = df.drop(columns=f"pheno_{disease}")

            # Create datasets
            X_train, X_test, y_train, y_test = train_test_split(df, phenotypes, test_size=0.25, stratify=phenotypes, random_state=seed)

            # Write to files
            if not os.path.exists(os.path.join(output_path, f"seed_{seed}/{disease}")):
                os.makedirs(os.path.join(output_path, f"seed_{seed}/{disease}"))
            X_train.to_csv(os.path.join(output_path, f"seed_{seed}/{disease}/X_train.csv"), index=False)
            X_test.to_csv(os.path.join(output_path, f"seed_{seed}/{disease}/X_test.csv"), index=False)
            y_train.to_csv(os.path.join(output_path, f"seed_{seed}/{disease}/y_train.csv"), index=False)
            y_test.to_csv(os.path.join(output_path, f"seed_{seed}/{disease}/y_test.csv"), index=False)


    for seed in [38565, 22656, 81660]:
        for disease in diseases:
            df = load_data_multi_prs(path, disease) # Load data with prs and phenotype of specified disease

            # Split off categorical data, normalize remaining data, merge again
            df, categorical_columns = split_off_categorical_data(df)
            df = normalize_min_max(df)
            df = df.join(categorical_columns)
           
            if disease == "bc":
                df = female_only_for_bc_data(df)

            # Split off phenotype of investigated disease
            phenotypes = df[f"pheno_{disease}"]
            df = df.drop(columns=f"pheno_{disease}")

            # Create datasets
            X_train, X_test, y_train, y_test = train_test_split(df, phenotypes, test_size=0.25, stratify=phenotypes, random_state=seed)

            # Write to files
            if not os.path.exists(os.path.join(output_path, f"seed_{seed}/{disease}")):
                os.makedirs(os.path.join(output_path, f"seed_{seed}/{disease}"))
            X_train.to_csv(os.path.join(output_path, f"seed_{seed}/{disease}/X_train.csv"), index=False)
            X_test.to_csv(os.path.join(output_path, f"seed_{seed}/{disease}/X_test.csv"), index=False)
            y_train.to_csv(os.path.join(output_path, f"seed_{seed}/{disease}/y_train.csv"), index=False)
            y_test.to_csv(os.path.join(output_path, f"seed_{seed}/{disease}/y_test.csv"), index=False)
