import numpy as np
import pandas as pd


def augment_upsampling(X_train, Y_train, verbose=False):
    X_control = X_train[Y_train==0] 
    X_case = X_train[Y_train==1]
    Y_control = Y_train[Y_train==0] 
    Y_case = Y_train[Y_train==1]

    indices = np.random.randint(0, len(X_case), size=len(X_control))
    X_case_upsampled = X_case.iloc[indices]
    Y_case_upsampled = Y_case.iloc[indices]

    X_train_aug = pd.concat([X_control, X_case_upsampled])
    Y_train_aug = pd.concat([Y_control, Y_case_upsampled])

    return  X_train_aug, Y_train_aug


def normalize_min_max(df):
    df -= df.min()
    df /= df.max()
    return df

