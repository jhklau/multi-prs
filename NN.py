from copy import deepcopy
import pandas as pd
import os
from sklearn import metrics
from sklearn.model_selection import StratifiedKFold
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch.optim as optim
import numpy as np
from statistics import mean
import time
from utils import augment_upsampling

def load_data(path):
    X_train = pd.read_csv(os.path.join(path, "X_train.csv"))
    X_test =  pd.read_csv(os.path.join(path, "X_test.csv"))
    y_train = pd.read_csv(os.path.join(path, "y_train.csv"), squeeze=True) # squeeze = True -> load as Series instead of DataFrame
    y_test =  pd.read_csv(os.path.join(path, "y_test.csv"), squeeze=True)

    return X_train, X_test, y_train, y_test

class Net(nn.Module):
    def __init__(self, len_input):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(len_input, 16)
        self.fc2 = nn.Linear(16, 8)
        self.fc3 = nn.Linear(8, 4)
        self.fc4 = nn.Linear(4, 1)

    # def __init__(self, len_input):
    #     super(Net, self).__init__()
    #     self.fc1 = nn.Linear(len_input, 64)
    #     self.fc2 = nn.Linear(64, 32)
    #     self.fc3 = nn.Linear(32, 8)
    #     self.fc4 = nn.Linear(8, 1)

#    def __init__(self, len_input):
#        super(Net, self).__init__()
#        self.fc1 = nn.Linear(len_input, 128)
#        self.fc2 = nn.Linear(128, 64)
#        self.fc3 = nn.Linear(64, 32)
#        self.fc4 = nn.Linear(32, 1)


    def forward(self, x):
        x = torch.flatten(x, 1)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = torch.sigmoid(self.fc4(x))
        return x


class CustomDataset(Dataset):
    def __init__(self, features, labels):
        self.labels = torch.from_numpy(labels.values.astype(np.float32))
        self.features = torch.from_numpy(features.values.astype(np.float32))

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        # print(self.features[idx])
        features = self.features[idx]
        label = self.labels[idx]
        return (features, label)       



if __name__ == "__main__":
    
    timestamp = int(time.time())
    results_dir = "/RESULTS/DIR/NN/"
    result_file = results_dir + f"NN_results_{timestamp}.csv"
    # if not os.path.exists(result_file):

    with open(result_file, "a") as file:
        tmp_model = Net(999999)
        file.write(tmp_model.__str__()+"\n")

    for prs_mode in ["single_prs", "multi_prs"]:
        for seed in [38565, 22656, 81660]:
            for disease in ["cad", "bc", "t2d"]:
                results_dir = f"/RESULTS/DIR/NN/{prs_mode}_{disease}_{seed}"
                if not os.path.exists(results_dir):
                    os.makedirs(results_dir)

                batch_size = 200
                num_epochs = 100
                learning_rate = 0.0001

                X_train, X_test, y_train, y_test = load_data(f"/PATH/TO/SPLIT/DATA/{prs_mode}/seed_{seed}/{disease}")

                skf = StratifiedKFold(n_splits=10, random_state=seed, shuffle=True)
                results = {}

                # Cross validation
                foldnum = 0
                for train_indices, val_indices in skf.split(X_train, y_train):
                    best_model_in_fold = ()
                    X_tain_fold = X_train.iloc[train_indices].copy()
                    y_train_fold = y_train.iloc[train_indices].copy()
                    X_val_fold = X_train.iloc[val_indices].copy()
                    y_val_fold = y_train.iloc[val_indices].copy()

                    X_train_fold_augmented, y_train_fold_augmented = augment_upsampling(X_tain_fold, y_train_fold)

                    # Create data sets and loaders
                    ds_train = CustomDataset(X_train_fold_augmented, y_train_fold_augmented)
                    ds_val = CustomDataset(X_val_fold, y_val_fold)
                    train_dataloader = DataLoader(ds_train, batch_size=batch_size, shuffle=True)
                    val_dataloader = DataLoader(ds_val, shuffle=True)


                    # Initialize neural network
                    torch.manual_seed(seed)
                    model = Net(X_train.shape[1])
                    

                    # Initialize optimizer and loss funcition
                    optimizer = optim.Adam(model.parameters(), lr=learning_rate)
                    loss = torch.nn.BCELoss() # Binary cross entropy for classification taask

                    # Initialize lists to save losses and AUC values
                    train_loss_log = []
                    val_loss_log = []
                    val_auc_log = []

                    # Training
                    for epoch in range(1, num_epochs + 1):
                        epoch_loss = 0
                        model.train()

                        # Provide batches to the network
                        for i,(features, labels) in enumerate(train_dataloader):
                            outputs = model(features).squeeze()
                            batch_loss = loss(outputs, labels)

                            optimizer.zero_grad()
                            batch_loss.backward()
                            optimizer.step()
                            epoch_loss += batch_loss.detach().numpy()
                        print(f"Epoch {epoch} Loss: {epoch_loss/len(train_dataloader)}")

                        # Validation
                        model.eval()
                        val_outputs = model(ds_val.features).squeeze()
                        val_loss = loss(val_outputs, ds_val.labels)
                        print(f"Epoch {epoch} Val Loss: {val_loss}")
                        
                        fpr, tpr, thresholds = metrics.roc_curve(ds_val.labels, val_outputs.detach().numpy(), pos_label=1)
                        auc = metrics.auc(fpr, tpr)
                        print(f"AUC_val: {auc}")

                        


                        if len(best_model_in_fold) == 0 or auc > best_model_in_fold[0] :
                            model_copy = deepcopy(model)
                            best_model_in_fold = (auc, model_copy)
                            print("Replacing model in best_model_in_fold")


                    results[best_model_in_fold[0]] = best_model_in_fold[1] 

                    model.eval()
                    val_outputs = best_model_in_fold[1](ds_val.features).squeeze()
                    val_loss = loss(val_outputs, ds_val.labels)
                    print(f"Epoch {epoch} Val Loss: {val_loss}")
                        

                    raw_results = pd.DataFrame(columns=["y_val", "predictions"])
                    raw_results["y_val"] = y_train.iloc[val_indices]
                    raw_results["predictions"] = val_outputs.detach().numpy()
                    
                    raw_results.to_csv(os.path.join(results_dir, f"raw_results_fold{foldnum}.csv"))

                    roc_results = pd.DataFrame(columns=["fpr", "tpr", "thresholds"])
                    roc_results["fpr"] = fpr
                    roc_results["tpr"] = tpr
                    roc_results["thresholds"] = thresholds 

                    roc_results.to_csv(os.path.join(results_dir, f"roc_results_fold{foldnum}.csv"))

                    foldnum += 1


                mean_val_auc = mean(results.keys())                

                with open(result_file, "a") as file:
                    file.write(f"{prs_mode},{seed},{disease},{mean_val_auc}\n")



