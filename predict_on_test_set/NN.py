from copy import deepcopy
import pandas as pd
import os
from sklearn import metrics
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset
from torch.utils.data import DataLoader
import torch.optim as optim
import numpy as np
from statistics import mean
import time
from utils import augment_upsampling

def load_data(path):
    X_train = pd.read_csv(os.path.join(path, "X_train.csv"))
    X_test =  pd.read_csv(os.path.join(path, "X_test.csv"))
    y_train = pd.read_csv(os.path.join(path, "y_train.csv"), squeeze=True) # squeeze = True -> load as Series instead of DataFrame
    y_test =  pd.read_csv(os.path.join(path, "y_test.csv"), squeeze=True)

    return X_train, X_test, y_train, y_test

class Net(nn.Module):
    def __init__(self, len_input):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(len_input, 8)
        self.fc2 = nn.Linear(8, 4)
        self.fc3 = nn.Linear(4, 4)
        self.fc4 = nn.Linear(4, 1)

    # def __init__(self, len_input):
    #     super(Net, self).__init__()
    #     self.fc1 = nn.Linear(len_input, 16)
    #     self.fc2 = nn.Linear(16, 8)
    #     self.fc3 = nn.Linear(8, 4)
    #     self.fc4 = nn.Linear(4, 1)


    def forward(self, x):
        x = torch.flatten(x, 1)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = torch.sigmoid(self.fc4(x))
        return x


class CustomDataset(Dataset):
    def __init__(self, features, labels):
        self.labels = torch.from_numpy(labels.values.astype(np.float32))
        self.features = torch.from_numpy(features.values.astype(np.float32))

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        features = self.features[idx]
        label = self.labels[idx]
        return (features, label)       



if __name__ == "__main__":
    
    timestamp = int(time.time())
    results_dir = f"/RESULTS/DIR/NN"
    if not os.path.exists(results_dir):
        os.makedirs(results_dir)
                
    result_file = os.path.join(results_dir, f"NN_results_{timestamp}.csv")


    for prs_mode in ["single_prs", "multi_prs"]:
        for seed in [38565, 22656, 81660]:
            for disease in ["cad", "bc", "t2d"]:
                raw_results_file = os.path.join(results_dir, f"raw_results_{prs_mode}_{disease}_{seed}.csv") 
                

                batch_size = 200
                num_epochs = 100
                learning_rate = 0.0001

                X_train, X_test, y_train, y_test = load_data(f"/PATH/TO/SPLIT/DATA/{prs_mode}/seed_{seed}/{disease}")

                X_train_fold_augmented, y_train_fold_augmented = augment_upsampling(X_train, y_train)

                # Create data sets and loaders
                ds_train = CustomDataset(X_train_fold_augmented, y_train_fold_augmented)
                train_dataloader = DataLoader(ds_train, batch_size=batch_size, shuffle=True)


                # Initialize neural network
                torch.manual_seed(seed)
                model = Net(X_train.shape[1])
                

                # Initialize optimizer and loss funcition
                optimizer = optim.Adam(model.parameters(), lr=learning_rate)
                loss = torch.nn.BCELoss() # Binary cross entropy for classification taask

                # Training
                for epoch in range(1, num_epochs + 1):
                    batch_loss_sum = 0
                    model.train()

                    # Provide batches to the network
                    for i,(features, labels) in enumerate(train_dataloader):
                        outputs = model(features).squeeze()
                        batch_loss = loss(outputs, labels)

                        optimizer.zero_grad()
                        batch_loss.backward()
                        optimizer.step()
                        batch_loss_sum += batch_loss.detach().numpy()
                    epoch_loss = batch_loss_sum/len(train_dataloader)
                    print(f"Epoch {epoch} Loss: {epoch_loss}")

                # Evaluation
                ds_test = CustomDataset(X_test, y_test)
                test_dataloader = DataLoader(ds_test, shuffle=True)

                model.eval()
                test_outputs = model(ds_test.features).squeeze()
                test_loss = loss(test_outputs, ds_test.labels)
                print(f"Test Loss: {test_loss}")

                raw_results = pd.DataFrame(columns=["y_test", "predictions"])
                raw_results["y_val"] = y_test
                raw_results["predictions"] = test_outputs.detach().numpy()
                
                raw_results.to_csv(os.path.join(results_dir, f"raw_results_{prs_mode}_{disease}_{seed}.csv"))


                fpr, tpr, thresholds = metrics.roc_curve(ds_test.labels, test_outputs.detach().numpy(), pos_label=1)
                auc_test = metrics.auc(fpr, tpr)
                print(f"AUC_test: {auc_test}")

                results_dir = "/RESULTS/DIR/NN"
                result_file = os.path.join(results_dir, f"NN_results_{timestamp}.csv")

                roc_results = pd.DataFrame(columns=["fpr", "tpr", "thresholds"])
                roc_results["fpr"] = fpr
                roc_results["tpr"] = tpr
                roc_results["thresholds"] = thresholds 

                roc_results.to_csv(os.path.join(results_dir, f"roc_results_{prs_mode}_{disease}_{seed}.csv"))

                with open(result_file, "a") as file:
                    file.write(f"{prs_mode},{seed},{disease},{auc_test}\n")



