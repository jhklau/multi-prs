import pandas as pd
import os
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier 
import time
from utils import augment_upsampling

def load_data(path):
    X_train = pd.read_csv(os.path.join(path, "X_train.csv"))
    X_test =  pd.read_csv(os.path.join(path, "X_test.csv"))
    y_train = pd.read_csv(os.path.join(path, "y_train.csv"), squeeze=True) # squeeze = True -> load as Series instead of DataFrame
    y_test =  pd.read_csv(os.path.join(path, "y_test.csv"), squeeze=True)

    return X_train, X_test, y_train, y_test



if __name__ == "__main__":
    timestamp = int(time.time())
    results_dir = f"/RESULTS/DIR/RF"
    if not os.path.exists(results_dir):
        os.makedirs(results_dir)

    metrics_file = os.path.join(results_dir, "metrics.csv")

    with open(metrics_file, "a") as file:
        file.write(f"prs_mode,seed,disease,n_estimators,auc\n")
    

    for prs_mode in ["single_prs", "multi_prs"]:
        for seed in [38565, 22656, 81660]:
            for disease in ["cad", "bc", "t2d"]:
    
                raw_results_file = os.path.join(results_dir, f"raw_results_{prs_mode}_{disease}_{seed}.csv")     
            
                # Create data sets
                X_train, X_test, y_train, y_test = load_data(f"/PATH/TO/SPLIT/DATA/{prs_mode}/seed_{seed}/{disease}")                                     
                X_train_fold_augmented, y_train_fold_augmented = augment_upsampling(X_train, y_train)

                # Train Model
                rf_model = RandomForestClassifier(n_estimators = 500, n_jobs=15)
                rf_model.fit(X_train_fold_augmented, y_train_fold_augmented)    
                
                print("predicting")
                # Evaluation
                predictions_proba = rf_model.predict_proba(X_test)
                predictions_proba = [item[1] for item in predictions_proba] # Get probabilties of target class
                fpr, tpr, thresholds = metrics.roc_curve(y_test, predictions_proba, pos_label=1)
                auc_test = metrics.auc(fpr, tpr)
                print(f"AUC_test: {auc_test}")

                roc_results = pd.DataFrame(columns=["fpr", "tpr", "thresholds"])
                roc_results["fpr"] = fpr
                roc_results["tpr"] = tpr
                roc_results["thresholds"] = thresholds 

                roc_results.to_csv(os.path.join(results_dir, f"roc_results__{prs_mode}_{disease}_{seed}.csv"))

                with open(metrics_file, "a") as file:
                        file.write(f"{prs_mode},{seed},{disease},{500},{auc_test}\n")

                raw_results = pd.DataFrame(columns=["y_test","predictions"])
                raw_results["y_test"] = y_test
                raw_results["predictions"] = predictions_proba

                raw_results.to_csv(raw_results_file)
