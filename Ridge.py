import pandas as pd
import os
from sklearn import metrics
from sklearn.linear_model import Ridge
from sklearn.model_selection import StratifiedKFold
from statistics import mean
from utils import augment_upsampling

def load_data(path):
    X_train = pd.read_csv(os.path.join(path, "X_train.csv"))
    X_test =  pd.read_csv(os.path.join(path, "X_test.csv"))
    y_train = pd.read_csv(os.path.join(path, "y_train.csv"), squeeze=True) # squeeze = True -> load as Series instead of DataFrame
    y_test =  pd.read_csv(os.path.join(path, "y_test.csv"), squeeze=True)

    return X_train, X_test, y_train, y_test



if __name__ == "__main__":
    for prs_mode in ["single_prs", "multi_prs"]:
        for seed in [38565, 22656, 81660]:
            for disease in ["cad", "bc", "t2d"]:
                for max_iter in [500]:
                    results_dir = f"/RESULTS/DIR/Ridge/{prs_mode}_{disease}_{seed}"
                    if not os.path.exists(results_dir):
                        os.makedirs(results_dir)
                
                    X_train, X_test, y_train, y_test = load_data(f"/PATH/TO/SPLIT/DATA/{prs_mode}/seed_{seed}/{disease}")

                    skf = StratifiedKFold(n_splits=10, random_state=seed, shuffle=True)
                    results = {}

                    foldnum = 0
                    for train_indices, val_indices in skf.split(X_train, y_train):
                        # Create data sets
                        X_tain_fold = X_train.iloc[train_indices].copy()
                        y_train_fold = y_train.iloc[train_indices].copy()

                        X_train_fold_augmented, y_train_fold_augmented = augment_upsampling(X_tain_fold, y_train_fold)

                        # Train Model
                        ridge_model = Ridge(max_iter=max_iter).fit(X_train_fold_augmented, y_train_fold_augmented.values.ravel())
                        
                        # Evaluation
                        predictions = ridge_model.predict(X_train.iloc[val_indices])
                        fpr, tpr, thresholds = metrics.roc_curve(y_train.iloc[val_indices], predictions, pos_label=1)
                        auc = metrics.auc(fpr, tpr)
                        results[auc] = ridge_model
                        print(f"AUC_val: {auc}")

                        raw_results = pd.DataFrame(columns=["y_val", "predictions"])
                        raw_results["y_val"] = y_train.iloc[val_indices]
                        raw_results["predictions"] = predictions
                        
                        raw_results.to_csv(os.path.join(results_dir, f"raw_results_fold{foldnum}.csv"))

                        roc_results = pd.DataFrame(columns=["fpr", "tpr", "thresholds"])
                        roc_results["fpr"] = fpr
                        roc_results["tpr"] = tpr
                        roc_results["thresholds"] = thresholds 

                        roc_results.to_csv(os.path.join(results_dir, f"roc_results_fold{foldnum}.csv"))

                        foldnum += 1


                    mean_val_auc = mean(results.keys())
                    
                    result_file = os.path.join(os.path.dirname(results_dir), "Ridge_results.csv")

                    with open(result_file, "a") as file:
                        file.write(f"{prs_mode},{seed},{disease},{max_iter},{mean_val_auc}\n")
