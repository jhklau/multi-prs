# AI-based multi-PRS models outperform classical single-PRS models

## Data Preparation
#### create_datasets.py
Creates multiple datasets for different diseases, seeds and prs-modes (singe, multi).

## Hyperparameter Tuning
#### NN.py, RF.py, RR.py
Used for hyperparameter tuning.  
Performs cross-validation for every permutation of disease, seed, prs-mode and chosen hyperparameters.  
Output: mean AUC over all cross-valiation folds

## Evaluation
#### /predict_on_test/[NN.py | RF.py | RR.py]
Used for evaluation on unseen test set.  
Trains a model on all data in the train set and performs a prediction on the test set.  
Output: AUC of predictions on test set

## Additional ouputs
ROC results (fpr, tpr, thresholds) will be saved in *roc_results_{prs_mode}_{disease}_{seed}.csv*

Prediction and ground truth for every element in the test set will be saved in *raw_results_{prs_mode}_{disease}_{seed}.csv*

